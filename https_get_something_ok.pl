#!perl
use strict;
use warnings;
use Mojo::UserAgent;
use feature 'say';

my $url = "https://www.google.com";
print "Fetching $url\n";

my $mu = Mojo::UserAgent->new;
my $res = $mu->get($url)->result;
if ($res->is_success) {
    say "ok: " . $res->body;
} elsif ($res->is_error) { 
    say "error: " . $res->message;
} else {
    say "bad: " . $res->code;
}

