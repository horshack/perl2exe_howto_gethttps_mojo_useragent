rem clean up perl 
PATH=C:\WINDOWS\system32;C:\WINDOWS;c:\strawberry\perl\site\bin;c:\strawberry\perl\bin;c:\strawberry\perl\vendor\bin

set MODS_SSL=IO::Socket::SSL

rem you can compile more programs like this:
rem set FILES2COMPILE=firstname secondname thirdname
set FILES2COMPILE=https_get_something_ok

rem variable in FOR is only allowed with a single letter
FOR %%B IN (%FILES2COMPILE%) DO (
    rem delete old exe-file
    IF EXIST %%B.exe del %%B.exe

    rem pack it
    rem zlib, ...libeay..., ...ssleay... are needed
    rem for Mojo::UserAgent -> IO::Socket::SSL which fetches the https-pages
    call pp ^
        -I c:/strawberry/perl/site/lib ^
        -I c:/strawberry/perl/lib ^
        -I c:/strawberry/perl/vendor/lib ^
        -M %MODS_SSL% ^
        -l C:/Strawberry/c/bin/zlib1_.dll ^
        -l C:/Strawberry/c/bin/libeay32_.dll ^
        -l C:/Strawberry/c/bin/ssleay32_.dll ^
        --output=%%B.exe %%B.pl

    rem run it
    rem the path to put my temporary PAR::Packer files to (relative path is ok)
    rem with setting this variable PAR_GLOBAL_TEMP I am able to delete 
    rem the temporary files when I am in dought
    set PAR_GLOBAL_TEMP=.\temp
    .\%%B.exe
)

