# File

./perl2exe_howto_gethttps/documentation/prepare_win10_env_for_perl2exe_howto_gethttps.md

# Author, Versions

Richard Lippmann, lippmann@findus-internet-opac.de, 6.1.2019

# Mission

I need to fetch websites with https. Perlprogram to be "compiled"
with PAR::Packer/pp under Windows to let the programm run on
customers machines which have no Strawberry Perl installed like this

```bat
set PAR_GLOBAL_TEMP=c:\whatever\temp
fetch_something_via_https.exe
```

This is the documentation howto prepare the environment in Windows 10.

# My struggle

I had a hard time to achive this because the wonderful pp/PAR::Packer
in Strawberry Perl is not creating an exe-file which works on a machine
where no Strawberry Perl is installed.

# Thanks

Thanks to all the people working for perl which helps me making
my work happen!

# Environment

- Windows 10, 64 Bit
- Strawberry Perl 32 Bit
- Fetch https-websites with Mojo::UserAgent in a perl-programm
- perl-program must be compiled with pp (PAR::Packer)

# Installing Strawberry Perl

Newest version 5.28.1.1 is not able to install PAR::Packer with then command
"cpan PAR::Packer". I get errors. Why errors? Look at cpantesters
http://matrix.cpantesters.org/?dist=PAR%3A%3APacker

The next version on this webpage showing me a perl version which works
with perl + PAR::Packer ist Strawberry Perl V5.20.3

So I install Strawberry perl v5.20.3.3 32 Bit MSI package

# Install PAR::Packer

I do not clean up PATH-Variable in any way.

```bat
cpan PAR::Packer
```

This takes a long time because TK-modules are also installed.

# Install newst Mojolicious

In Strawberry perl now installed is Mojolicious 6.53, but I need the newest
version for Mojo::UserAgent because method "res" has been changed to "result".
This command installs everything to c:\strawberry\perl\site

```bat
cpan Mojolicious
```

Move the old versions of Mojolicious and Mojo from strawberry perl
out of the way. Without that running "perl myfile.pl" is working with
the newest Mojo::UserAgent, but the compiled myfile.exe has
the old Mojo::UserAgent included and runs into an error when calling
the new method Mojo::UserAgent::result

```bat
rename C:\Strawberry\perl\vendor\lib\Mojo nono_Mojo
rename C:\Strawberry\perl\vendor\lib\Mojolicious nono_Mojolicious
```

# Build a testenvironment

```bat
mkdir c:\temp\perl2exetest
mkdir c:\temp\perl2exetest\temp
mkdir c:\temp\perl2exetest\t
```

Create a test-file to test the perl-modules.

```perl
# file t\000-perlmodules-needed.t
use strict;
use warnings;
use Test::More;

use_ok('PAR::Packer');
use_ok('Mojolicious', 8.11);
use_ok('Mojo::Transaction');
can_ok('Mojo::Transaction', 'result');

done_testing();
```

Let the test run

```bat
prove -v

t\000-perlmodules-needed.t ..
ok 1 - use PAR::Packer;
ok 2 - use Mojolicious;
ok 3 - use Mojo::Transaction;
ok 4 - Mojo::Transaction->can('result')
1..4
ok
All tests successful.
Files=1, Tests=4,  2 wallclock secs ( 0.02 usr +  0.05 sys =  0.06 CPU)
Result: PASS
```

# Build a testfile which fetches https

File c:\temp\perl2exetest\https_get_something_ok.pl

```perl
#!perl
use strict;
use warnings;
use Mojo::UserAgent;
use feature 'say';

my $url = "https://www.google.com";
print "Fetching $url\n";

my $mu = Mojo::UserAgent->new;
my $res = $mu->get($url)->result;
if ($res->is_success) {
    say "ok: " . $res->body;
} elsif ($res->is_error) {
    say "error: " . $res->message;
} else {
    say "bad: " . $res->code;
}
```

Let the perl-file run, this should work

```bat
perl.exe https_get_something_ok.pl
```

# Create a batchfile to pack the perl-program to an exe-file

File c:\temp\perl2exetest\compile_all_with_strawberry.bat

```bat
rem clean up perl
PATH=C:\WINDOWS\system32;C:\WINDOWS;c:\strawberry\perl\site\bin;c:\strawberry\perl\bin;c:\strawberry\perl\vendor\bin

set MODS_SSL=IO::Socket::SSL

rem you can compile more programs like this:
rem set FILES2COMPILE=firstname secondname thirdname
set FILES2COMPILE=https_get_something_ok

rem variable in FOR is only allowed with a single letter
FOR %%B IN (%FILES2COMPILE%) DO (
    rem delete old exe-file
    IF EXIST %%B.exe del %%B.exe

    rem pack it
    rem -I
    rem   find perl-modules in these directories
    rem -M
    rem   add perl-modules which get not found from PAR::Packer
    rem   but needed (usually dynamically loaded perl-modules)
    rem -l
    rem   zlib, ...libeay..., ...ssleay... are needed
    rem   for Mojo::UserAgent -> IO::Socket::SSL which fetches the https-pages
    call pp ^
        -I c:/strawberry/perl/site/lib ^
        -I c:/strawberry/perl/lib ^
        -I c:/strawberry/perl/vendor/lib ^
        -M %MODS_SSL% ^
        -l C:/Strawberry/c/bin/zlib1_.dll ^
        -l C:/Strawberry/c/bin/libeay32_.dll ^
        -l C:/Strawberry/c/bin/ssleay32_.dll ^
        --output=%%B.exe %%B.pl

    rem run it
    rem the path to put my temporary PAR::Packer files to (relative path is ok)
    rem with setting this variable PAR_GLOBAL_TEMP I am able to delete
    rem the temporary files when I am in dought
    set PAR_GLOBAL_TEMP=.\temp
    .\%%B.exe
)
```

Let the batchfile compile_all_with_strawberry.bat run to create an exe-file.

The file https_get_something_ok.exe should exist now. Let it run, it should
fetch the https-page.

# Test it on a non-perl-Windows-machine

Transfer https_get_something_ok.exe to a machine where no perl is
installed and let it run there.
