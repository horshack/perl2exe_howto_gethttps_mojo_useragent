use strict;
use warnings;
use Test::More;

use_ok('PAR::Packer');
use_ok('Mojolicious', 8.11);
use_ok('Mojo::Transaction');
can_ok('Mojo::Transaction', 'result');

done_testing();